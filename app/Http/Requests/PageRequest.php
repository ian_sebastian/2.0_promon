<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'permalink' => 'sometimes|required|unique:pages,permalink,' . $this->id,
            'url' => 'nullable',
            'content' => 'required',
            'ogp_title' => 'nullable',
            'ogp_type' => 'nullable',
            'ogp_url' => 'nullable|active_url',
            'ogp_image' => ['nullable', 'url', 'regex:~^https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\.(?:jpe?g|gif|png)$~'],
            'ogp_site_name' => 'nullable',
            'ogp_description' => 'nullable',
            'social_fb_app_id' => 'nullable',
            'social_fb_admin_id' => 'nullable',
            'social_twitter_title' => 'nullable',
            'social_twitter_image' => ['nullable', 'url', 'regex:~^https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\.(?:jpe?g|gif|png)$~'],
            'social_twitter_card' => 'nullable',
            'meta_title' => 'nullable',
            'meta_description' => 'nullable',
            'meta_keyword' => 'nullable',
            'is_published' => 'nullable|boolean',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'タイトルを入力してください',
            'permalink.required' => 'パーマリンクを入力してください',
            'content.required' => 'コンテンツフィールドは必須です',
            'active_url' => '無効URL',
            'url' => '無効URL',
            'regex' => '無効URL',
        ];
    }
}
