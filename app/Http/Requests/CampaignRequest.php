<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class CampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'released_start_at' => 'nullable|date|before_or_equal:released_end_at',
            'released_end_at' => 'nullable|date|after:released_start_at',
            'finish_redirect_url' => 'required|url',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'キャンペーン名を入力してくだ さい',
        ];
    }

    /**
     * Get data that apply to the request.
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => $this->name,
            'released_start_at' => Carbon::parse($this->released_start_at),
            'released_end_at' => Carbon::parse($this->released_end_at),
            'finish_redirect_url' => $this->finish_redirect_url,
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl() . '#campaign');
    }
}
