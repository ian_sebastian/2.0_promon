<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'code' => 'nullable',
            'content' => 'nullable',
            'questions.*.is_required' => 'nullable',
            'questions.*.has_others' => 'nullable',
            'questions.*.question' => 'required',
            'questions.*.type' => 'required',
            'questions.*.master_input_id' => 'required',
            'questions.*.before_text' => 'nullable',
            'questions.*.after_text' => 'nullable',
            'admin_is_notify' => 'required',
            'admin_subject' => 'required_if:admin_is_notify,1',
            'admin_emails.*' => 'sometimes|nullable|required_if:admin_is_notify,1|email',
            'user_is_notify' => 'required',
            'user_subject' => 'required_if:user_is_notify,1',
            'user_emails.*' => 'sometimes|nullable|required_if:user_is_notify,1|email',
            'user_email_content' => 'required_if:user_is_notify,1',
        ];
    }

    /**
     * Group and Process the data needed to save in the database.
     *
     * @return array
     */
    public function data()
    {
        $form = $this->only('title');

        $questionnaire = collect($this->only('questions'))->flatMap(function ($value) {
            return $value;
        })->values();

        $config = $this->only(
            [
                'form_id',
                'admin_is_notify',
                'admin_subject',
                'admin_emails',
                'user_is_notify',
                'user_subject',
                'user_emails',
                'user_email_content',
            ]
            );

        return compact('form', 'questionnaire', 'config');
    }
}
