<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray(
            [
                'title' => $request['title'],
                'permalink' => $request['permalink'],
                'content' => $request['content'],
                'ogp_title' => $request['ogp_title'],
                'ogp_type' => $request['ogp_type'],
                'ogp_url' => $request['ogp_url'],
                'ogp_image' => $request['ogp_image'],
                'ogp_site_name' => $request['ogp_site_name'],
                'ogp_description' => $request['ogp_description'],
                'social_fb_app_id' => $request['social_fb_app_id'],
                'social_fb_admin_id' => $request['social_fb_admin_id'],
                'social_twitter_title' => $request['social_twitter_title'],
                'social_twitter_image' => $request['social_twitter_image'],
                'social_twitter_card' => $request['social_twitter_card'],
                'meta_title' => $request['meta_title'],
                'meta_description' => $request['meta_description'],
                'meta_keyword' => $request['meta_keyword'],
                'is_published' => $request['is_published'],
            ]
        );
    }
}
