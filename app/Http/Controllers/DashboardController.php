<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Form;
use App\Models\Page;
use App\Models\SubmittedForm;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param null|mixed $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id = null)
    {
        $forms = Form::all();
        $campaign = Campaign::first();
        $pages = Page::whereBetween('created_at', [$campaign->released_start_at, $campaign->released_end_at])->get();

        $submittedForms = SubmittedForm::with(['form'])->SubmittedForms()->where('form_id', $id)->get();

        $submittedFormGroupedDates = $submittedForms->groupBy(function ($data) {
            return $data->created_at->format('m月d日');
        });

        $labels = $submittedFormGroupedDates->keys();

        $data = $submittedFormGroupedDates->map(function ($value) {
            return $value->count();
        })->values();

        $analytics = [
            'labels' => $labels,
            'submittedForms' => $submittedForms->count(),
            'allSubmittedForms' => SubmittedForm::all()->count(),
        ];

        $datasets = collect([
            'label' => $submittedForms->first()->form->title ?? '',
            'borderColor' => 'rgba(39, 117, 246, 1)',
            'data' => $data,
        ]);

        return view('dashboard')->with([
            'forms' => $forms,
            'analytics' => $analytics,
            'campaign' => $campaign,
            'datasets' => [$datasets],
            'pages' => $pages,
        ]);
    }
}
