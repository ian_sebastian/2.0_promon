<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionsRequest;
use App\Http\Resources\FormCollection;
use App\Models\Form;
use App\Models\MasterInput;
use App\Services\FormService;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::all();

        return view('forms.index')
            ->with('forms', $forms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $old = $request->session()->get('_old_input');
        $form = new FormCollection($old);
        $masterInputs = MasterInput::all();

        return view('forms.create')
            ->with('masterInputs', $masterInputs)
            ->with('form', $form);
    }

    /**
     * Save the new Form in database.
     *
     * @param FormService      $formService
     * @param QuestionsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function save(FormService $formService, QuestionsRequest $request)
    {
        if (!$request->route('id')) {
            $formService->create($request->data());
        } else {
            $formService->update(array_merge(['id' => $request->route('id')], $request->data()));
        }

        return redirect()->route('forms.index');
    }

    /**
     * Get the details of specified form in database.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = Form::find($id);
        $form['questions'] = $form->questions->toArray();
        collect($form->config->toArray())->map(function ($value, $key) use ($form) {
            $form[$key] = $value;
        });
        $form['admin_emails'] = str_replace(['[', '"', ']'], '', explode(',', $form['admin_emails']));
        $form['user_emails'] = str_replace(['[', '"', ']'], '', explode(',', $form['user_emails']));
        $form = new FormCollection($form);

        $masterInputs = MasterInput::all();

        return view('forms.create')
            ->with('form', $form)
            ->with('masterInputs', $masterInputs);
    }

    /**
     * Bulk delete form.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        Form::whereIn('id', $request->id)->delete();

        return redirect()->route('forms.index');
    }
}
