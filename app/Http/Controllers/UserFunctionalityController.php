<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserFunctionalityImportRequest;
use App\Http\Requests\UserFunctionalityRequest;
use App\Imports\UserFunctionalityImport;
use App\Services\MemberService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class UserFunctionalityController extends Controller
{
    /**
     * Save created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param MemberService            $memberService
     * @return \Illuminate\Http\Response
     */
    public function save(UserFunctionalityRequest $request, MemberService $memberService)
    {
        $memberService->perform($request->except('_token'));
        Session::flash('success_member', '更新しました');

        return redirect()->to(route('settings.index') . '#members');
    }

    /**
     * Import the data from file to database.
     * @param UserFunctionalityImportRequest $request
     */
    public function import(UserFunctionalityImportRequest $request)
    {
        Excel::import(new UserFunctionalityImport, $request->file('file'));
        Session::flash('success_member', '更新しました');

        return redirect()->to(route('settings.index') . '#members');
    }
}
