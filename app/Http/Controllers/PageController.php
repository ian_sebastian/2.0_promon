<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use App\Http\Resources\PageCollection;
use App\Models\Page;
use App\Services\PageService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();

        return view('pages.index')
            ->with('pages', $pages);
    }

    /**
     * Show the form for creating a new page.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $old = $request->session()->get('_old_input');
        $old['is_published'] = isset($old['is_published']) ? '1' : '';
        $page = new PageCollection($old);

        return view('pages.create')
            ->with('page', $page);
    }

    /**
     * Store a newly created page in database.
     *
     * @param \Illuminate\Http\Request $request
     * @param PageService              $service
     * @return \Illuminate\Http\Response
     */
    public function save(PageRequest $request, PageService $service)
    {
        if (!$request->route('id')) {
            $service->create($request->except('_token'));
        } else {
            $service->update(array_merge($request->except('_token'), ['id' => $request->route('id')]));
        }

        return redirect()->route('pages.index');
    }

    /**
     * Get the details of specified page in database.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);

        return view('pages.create')
            ->with('page', $page);
    }

    /**
     * Bulk delete page.
     *
     * @param Request     $request
     * @param NewsService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        Page::whereIn('id', $request->id)->delete();

        return redirect()->route('pages.index');
    }
}
