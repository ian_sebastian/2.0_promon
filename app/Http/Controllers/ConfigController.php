<?php

namespace App\Http\Controllers;

use App\Http\Resources\CampaignCollection;
use App\Http\Resources\IPCollection;
use App\Http\Resources\MasterFieldCollection;
use App\Http\Resources\MemberHeaderCollection;
use App\Models\Campaign;
use App\Models\MasterInput;
use App\Models\MemberHeader;
use App\Models\RestrictAddress;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaign = new CampaignCollection(Campaign::find(1));
        $masterFields = new MasterFieldCollection(MasterInput::all());
        $memberHeaders = new MemberHeaderCollection(MemberHeader::all());
        $restrictAddresses = new IPCollection(RestrictAddress::all());

        return view('settings')
            ->with([
                'campaign' => $campaign,
                'masterFields' => $masterFields,
                'memberHeaders' => $memberHeaders,
                'restrictAddresses' => $restrictAddresses,
            ]);
    }
}
