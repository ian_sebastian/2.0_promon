<?php

namespace App\Http\Controllers;

use App\Exports\ApplicantsExports;
use App\Services\ApplicantService;
use Maatwebsite\Excel\Facades\Excel;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param null|mixed       $id
     * @param ApplicantService $applicantService
     * @return \Illuminate\Http\Response
     */
    public function index($id = null, ApplicantService $applicantService)
    {
        $data = $applicantService->getApplicants($id);

        return view('applicants')->with($data);
    }

    /**
     * Export the data from database.
     *
     * @return \Illuminate\Http\Response
     */
    public function export()
    {
        return Excel::download(new ApplicantsExports, 'applicants' . date('Ymd_H.i.s') . '.csv');
    }
}
