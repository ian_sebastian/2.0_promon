<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use Illuminate\Http\Request;
use Session;

class CampaignController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Campaign                 $campaign
     * @return \Illuminate\Http\Response
     */
    public function store(Campaign $campaign, CampaignRequest $request)
    {
        $campaign->find(1)->fill($request->data())->save();
        Session::flash('success_campaign', '更新しました');

        return redirect()->to(route('settings.index') . '#campaign');
    }
}
