<?php

namespace App\Utils;

use Form;

trait FormUtil
{
    /**
     * Generate label to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function label($data)
    {
        return Form::label($data->name, null, ['placeholder' => $data->label]);
    }

    /**
     * Generate Input Text to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function text($data)
    {
        return Form::text($data->name, null, ['placeholder' => $data->label]);
    }

    /**
     * Generate Hidden Text to HTML.
     *
     * @param mixed $data
     * @param mixed $name
     * @param mixed $value
     * @return htmlString
     */
    public static function hidden($name, $value)
    {
        return Form::hidden($name, $value);
    }

    /**
     * Generate Input Email to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function email($data)
    {
        return Form::email($data->name, null, ['placeholder' => $data->label]);
    }

    /**
     * Generate Radio Button to HTML.
     *
     * @param mixed $data
     * @param mixed $value
     * @return htmlString
     */
    public static function radio($data, $value)
    {
        return Form::radio($data->name . '[value]', $value, false);
    }

    /**
     * Generate checkbox to HTML.
     *
     * @param mixed $data
     * @param mixed $value
     * @return htmlString
     */
    public static function checkbox($data, $value)
    {
        return Form::checkbox($data->name . '[value][]', $value, false);
    }

    /**
     * Generate select to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function select($data)
    {
        return Form::select($data->name, json_decode($data->options), null, ['placeholder' => $data->label]);
    }

    /**
     * Generate textarea to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function textarea($data)
    {
        return Form::textarea($data->name, null, ['placeholder' => $data->label]);
    }

    /**
     * Generate submit to HTML.
     *
     * @param mixed $data
     * @return htmlString
     */
    public static function submit()
    {
        return Form::submit();
    }
}
