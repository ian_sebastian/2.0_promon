<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionFormAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'submitted_form_id',
        'question_form_id',
        'answer',
        'other',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get the answers from the form.
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * The questions that belong to the Question_Form.
     */
    public function questions()
    {
        return $this->belongsTo(QuestionForm::class, 'question_form_id');
    }

    /**
     * The questions that belong to the Question_Form.
     */
    public function submitted()
    {
        return $this->belongsTo(QuestionForm::class);
    }
}
