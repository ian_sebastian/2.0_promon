<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberHeader extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field',
        'is_encrypted',
    ];
}
