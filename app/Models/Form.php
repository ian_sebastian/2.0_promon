<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'code',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * Get the questions from the form.
     */
    public function questions()
    {
        return $this->hasMany(QuestionForm::class);
    }

    /**
     * Get the questions from the form.
     */
    public function submittedForm()
    {
        return $this->hasMany(SubmittedForm::class);
    }

    /**
     * Get the config from the form.
     */
    public function config()
    {
        return $this->hasOne(ConfigForm::class);
    }
}
