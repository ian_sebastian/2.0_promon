<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigForm extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_id',
        'admin_is_notify',
        'admin_subject',
        'admin_emails',
        'user_is_notify',
        'user_subject',
        'user_emails',
        'user_email_content',
    ];

    /**
     * The Form that belongs to the Config_Form.
     */
    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    /**
     * Get the config from the form.
     */
    public function config()
    {
        return $this->hasOne(MasterInput::class);
    }
}
