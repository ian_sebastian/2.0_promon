<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'permalink',
        'url',
        'content',
        'ogp_title',
        'ogp_type',
        'ogp_url',
        'ogp_image',
        'ogp_site_name',
        'ogp_description',
        'social_fb_app_id',
        'social_fb_admin_id',
        'social_twitter_title',
        'social_twitter_image',
        'social_twitter_card',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'is_published',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at',
    ];
}
