<?php

namespace App\Imports;

use App\Services\MemberService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserFunctionalityImport implements ToCollection, WithHeadingRow
{
    /**
     * Get data from csv file.
     *
     * @param Collection $data
     * @return \Illuminate\Support\Collection
     */
    public function collection(Collection $data)
    {
        $results = $data->mapWithKeys(function ($item, $key) {
            return [($key) => collect([
                'field' => $item['field'],
                'is_encrypted' => (string) ($item['is_encrypted']),
            ])];
        });

        $memberservice = new MemberService;
        $memberservice->perform(collect($results));
    }
}
