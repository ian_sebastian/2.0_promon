<?php

namespace App\Services;

use App\Models\Member;
use App\Models\MemberHeader;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MemberService
{
    /**
     * Store created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $data
     * @return \Illuminate\Http\Response
     */
    public function perform($data)
    {
        //incoming data
        $incomingHeaders = collect($data)->map(function ($item) {
            return  json_encode([
                'field' => $item['field'],
                'is_encrypted' => $item['is_encrypted'],
            ]);
        });

        //existing data
        $existingHeaders = collect(MemberHeader::all())->map(function ($item) {
            return  json_encode([
                 'field' => $item['field'],
                 'is_encrypted' => $item['is_encrypted'],
             ]);
        });

        $insertedData = $incomingHeaders->diff($existingHeaders);
        $deletedData = $existingHeaders->diff($incomingHeaders);

        //backup data
        $member = collect(Member::all())->toArray();

        $backupData = collect($member)->map(function ($value, $key) use ($insertedData) {
            $insertedData = collect($insertedData)->map(function ($item) {
                return json_decode($item)->field;
            })->toArray();

            return collect($value)->only($insertedData)->toArray();
        });

        // delete Member headers
        collect($deletedData)->each(function ($item) {
            $item = json_decode($item);
            MemberHeader::where('field', $item->field)->delete();
        });

        $memberHeaders = collect($insertedData)->map(function ($item) {
            $item = json_decode($item);
            return [
                 'field' => $item->field,
                 'is_encrypted' => $item->is_encrypted,
             ];
        })->values();

        // insert or update new Member headers
        MemberHeader::insert($memberHeaders->toArray());

        //delete each column in Member Table
        collect($deletedData)->each(function ($item) {
            $item = json_decode($item);
            Schema::table('members', function (Blueprint $table) use ($item) {
                $table->dropColumn($item->field);
            });
        });

        Member::truncate();

        //Create each column in Members
        collect($insertedData)->each(function ($item) {
            $item = json_decode($item);
            Schema::table('members', function (Blueprint $table) use ($item) {
                $table->string($item->field)->nullable();
            });
        });

        // insert backup to Members
        if ($backupData->isNotEmpty()) {
            $members = $backupData->map(function ($data) use ($memberHeaders) {
                $value = $memberHeaders->mapWithKeys(function ($item) use ($data) {
                    $value = $data[$item['field']] ?? '';

                    if ((isset($value)) && !preg_match('/^\$2y\$/', $value)) {
                        $value = $item['is_encrypted'] ? '' : $value;
                    }

                    return [
                        $item['field'] => $value,
                    ];
                });

                return $value;
            });

            Member::insert($members->toArray());
        }
    }
}
