<?php

namespace App\Services;

use App\Models\ConfigForm;
use App\Models\Form;
use App\Models\MasterInput;
use App\Models\QuestionForm;
use App\Utils\FormUtil;
use Carbon\Carbon;
use Illuminate\Support\Str;

class FormService
{
    use FormUtil;

    /**
     * Store a newly created resource in database.
     *
     * @param $input
     * @param mixed $data
     */
    public function create($data)
    {
        $form = Form::create([
            'title' => $data['form']['title'],
        ]);

        $questions = collect($data['questionnaire'])->map(function ($value, $key) use ($form) {
            return [
                'form_id' => $form->id,
                'master_input_id' => $value['master_input_id'],
                'type' => $value['type'],
                'question' => $value['question'],
                'before_text' => $value['before_text'],
                'after_text' => $value['after_text'],
                'is_required' => $value['is_required'] ?? 0,
                'has_others' => $value['has_others'] ?? 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        })->all();

        QuestionForm::insert($questions);

        $form->fill([
            'code' => Str::replaceFirst('<<id>>', $form->id, '[contact-form-id=<<id>>]'),
        ])->save();

        ConfigForm::create([
            'form_id' => $form->id,
            'admin_is_notify' => $data['config']['admin_is_notify'],
            'admin_subject' => $data['config']['admin_subject'],
            'admin_emails' => json_encode($data['config']['admin_emails']),
            'user_is_notify' => $data['config']['user_is_notify'],
            'user_subject' => $data['config']['user_subject'],
            'user_emails' => json_encode($data['config']['user_emails']),
            'user_email_content' => $data['config']['user_email_content'],
        ]);
    }

    /**
     * Update the specified form in database.
     *
     * @param $input
     * @param $id
     * @param mixed $data
     */
    public function update($data)
    {
        $form = Form::findOrFail($data['id']);
        $questionForm = QuestionForm::where('form_id', $data['id'])->get();
        $configForm = ConfigForm::where('form_id', $data['id'])->first();

        $incoming = collect($data['questionnaire'])->map(function ($value) {
            return json_encode($value);
        })->values();

        $existing = $questionForm->map(function ($value) {
            return json_encode([
                'question' => $value->question,
                'form_id' => $value->form_id,
                'type' => $value->type,
                'master_input_id' => $value->master_input_id,
                'before_text' => $value->before_text,
                'has_others' => $value->has_others ?? '',
                'is_required' => $value->is_required ?? '',
                'after_text' => $value->after_text,
            ]);
        });

        $insertedData = $incoming->diff($existing);

        if ($existing->isNotEmpty()) {
            $deletedData = $existing->diff($incoming);

            // delete existing questions
            collect($deletedData)->each(function ($item) {
                $item = json_decode($item);
                QuestionForm::where('master_input_id', $item->master_input_id)->delete();
            });
        }

        // re-insert questions
        $questions = collect($insertedData)->map(function ($item) use ($form) {
            $item = json_decode($item);

            return [
                'form_id' => $form->id,
                'question' => $item->question,
                'type' => $item->type,
                'master_input_id' => $item->master_input_id,
                'before_text' => $item->before_text,
                'after_text' => $item->after_text,
                'is_required' => $item->is_required ?? 0,
                'has_others' => $item->has_others ?? 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        })->all();

        QuestionForm::insert($questions);

        $form->fill([
            'code' => Str::replaceFirst('<<id>>', $form->id, '[contact-form-id=<<id>>]'),
        ])->save();

        $data['config'] = [
                'admin_is_notify' => $data['config']['admin_is_notify'],
                'admin_subject' => $data['config']['admin_subject'],
                'admin_emails' => json_encode($data['config']['admin_emails']),
                'user_is_notify' => $data['config']['user_is_notify'],
                'user_subject' => $data['config']['user_subject'],
                'user_emails' => json_encode($data['config']['user_emails']),
                'user_email_content' => $data['config']['user_email_content'],
        ];

        $configForm->fill($data['config'])->save();
    }

    /**
     * Generate Form Code from the content.
     *
     * @todo Put the content of the form when the code is called.
     *
     * @param $input
     * @param $contents
     */
    public function generateFormCode($input, $contents)
    {
        $form = new Form();
        $questions = new QuestionForm();
        $new_content = collect($contents)->flatMap(function ($value) use ($form, $questions, $contents) {
            $questionForm = $form->find(preg_replace('/\D/', '', $value));
            $formContent = '';

            if ($questionForm !== null) {
                $questions = $questions->where('form_id', $questionForm->id)->get();
                $formContent = $this->generateContent($questionForm, $questions);
            }

            return [
                $value => $formContent,
            ];
        });
        return str_replace($new_content->keys()->toArray(), $new_content->values()->toArray(), $input['content']);
    }

    /**
     * Generate content to be stored in the database.
     *
     * @param $input
     * @param mixed $data
     * @param mixed $questionaires
     * @param mixed $questionaire
     * @param mixed $form
     * @param mixed $questions
     */
    public function generateContent($form, $questions)
    {
        $content = [];

        //<< start form >>
        array_push($content, '<form method="POST" name="form-' . $form->id . '">');

        //<< insert form id >>
        array_push($content, FormUtil::hidden('id', $form->id));

        $questions->each(function ($question) use (&$content) {
            $inputDetails = MasterInput::find($question['master_input_id']);
            $inputHTML = '';

            //<< start question section >>
            array_push($content, '<div>');

            //<< question >>
            isset($question['before_text']) ? array_push($content, '<div><i>' . $question['before_text'] . '</i></div>') : '';
            array_push($content, '<div>' . $question['question'] . '</div>');
            isset($question['after_text']) ? array_push($content, '<div><i>' . $question['after_text'] . '</i></div>') : '';

            //<< start input >>
            array_push($content, '<div>');

            switch ($question['type']) {
                case 'label': $inputHTML = FormUtil::label($inputDetails);
                break;
                case 'text': $inputHTML = FormUtil::text($inputDetails);
                break;
                case 'email': $inputHTML = FormUtil::email($inputDetails);
                break;
                case 'radio':
                    $contentGroup = [];

                    $options = json_decode($inputDetails->options);

                    //add others into options
                    if ($question['has_others']) {
                        array_push($options, 'other');
                    }

                    foreach ($options as $option) {
                        array_push($contentGroup, FormUtil::radio($inputDetails, $option) . $option);
                    }

                    //add others text field
                    if ($question['has_others']) {
                        $inputDetails->name = $inputDetails->name . '[other]';
                        array_push($contentGroup, FormUtil::text($inputDetails));
                    }

                    $inputHTML = implode($contentGroup);
                break;
                case 'checkbox':
                    $contentGroup = [];

                    $options = json_decode($inputDetails->options);

                    //add others into options
                    if ($question['has_others']) {
                        array_push($options, 'other');
                    }

                    foreach ($options as $option) {
                        array_push($contentGroup, FormUtil::checkbox($inputDetails, $option) . $option);
                    }

                    //add others text field
                    if ($question['has_others']) {
                        $inputDetails->name = $inputDetails->name . '[other]';
                        array_push($contentGroup, FormUtil::text($inputDetails));
                    }

                    $inputHTML = implode($contentGroup);
                break;
                case 'select': $inputHTML = FormUtil::select($inputDetails);
                break;
                case 'textarea': $inputHTML = FormUtil::textarea($inputDetails);
                break;
            }

            //<< input >>
            array_push($content, $inputHTML);

            //<< end input >>
            array_push($content, '</div>');

            //<< end question section >>
            array_push($content, '</div>');
        });

        //<< submit button >>
        array_push($content, '<div>' . FormUtil::submit() . '</div>');

        //<< end form >>
        array_push($content, '</form>');

        return implode($content);
    }
}
