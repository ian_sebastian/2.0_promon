<?php

namespace App\Services;

use App\Models\Member;
use App\Models\MemberHeader;
use Form;
use Illuminate\Support\Facades\Hash;

class MembersService
{
    /**
     * Store a newly created page in database.
     *
     * @param $input
     * @param mixed $data
     */
    public function create($data)
    {
        $member = new Member;
        $memberHeaders = MemberHeader::all();

        $input = collect($data)->mapWithKeys(function ($value, $key) use ($memberHeaders) {
            $fieldName = $key;
            $fieldValue = $value;

            collect($memberHeaders->toArray())->each(function ($value) use ($fieldName, &$fieldValue) {
                if ($value['field'] == $fieldName) {
                    $fieldValue = $value['is_encrypted'] == 1 ? Hash::make($fieldValue) : $fieldValue;
                }
            });

            return [$fieldName => $fieldValue];
        });

        collect($input)->each(function ($value, $key) use ($member) {
            $member->{$key} = $value;
        });

        $member->save();
    }

    /**
     * Update the specified member in database.
     *
     * @param $input
     */
    public function update($input)
    {
        $member = Member::findOrFail($input['id']);
        $memberHeaders = MemberHeader::all();

        $input = collect($input)->mapWithKeys(function ($value, $key) use ($memberHeaders) {
            $fieldName = $key;
            $fieldValue = $value;

            collect($memberHeaders->toArray())->each(function ($value) use ($fieldName, &$fieldValue) {
                if ($value['field'] == $fieldName) {
                    $fieldValue = $fieldValue !== null && $value['is_encrypted'] == 1 ? Hash::make($fieldValue) : $fieldValue;
                }
            });

            return [$fieldName => $fieldValue];
        });

        collect($input)->each(function ($value, $key) use ($member) {
            $member->{$key} = $value;
        });

        $member->save();
    }

    /**
     * Generate content to be show in member form.
     *
     * @param null|mixed $id
     */
    public function generateContent($id = null)
    {
        $member = !empty($id) ? Member::find($id) : new Member;
        $content = [];

        collect(array_except($member->getTableColumns(), 0))->each(function ($value) use (&$content, $member) {
            array_push($content, '<div class="tr member__page">');
            array_push($content, '<div class="th">' . $value . '</div>');
            array_push($content, '<div class="td">');

            if (isset($member->{$value}) && preg_match('/^\$2y\$/', $member->{$value})) {
                array_push($content, '<div class="encrypted__form">');
                array_push($content, Form::text($value, null, ['class' => 'encrypted__txt ' . $value, 'disabled']));
                array_push($content, Form::button('更新', ['class' => 'btn__container__member ' . $value, 'type' => 'button', 'id' => $value, 'onclick' => 'enableDisableButton(this.id)']));
                array_push($content, '</div>');
            } else {
                array_push($content, Form::text($value, $member->{$value} ?? null, ['class' => 'encrypted__txt ' . $value]));
            }
            array_push($content, '</div>');
            array_push($content, '</div>');
        });

        return implode($content);
    }
}
