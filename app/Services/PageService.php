<?php

namespace App\Services;

use App\Models\Page;

class PageService
{
    /**
     * Store a newly created page in database.
     *
     * @param $input
     */
    public function create($input)
    {
        $input['url'] = env('APP_URL') . '/' . $input['permalink'];
        $input['is_published'] = $input['is_published'] ?? 0;
        $input['content'] = html_entity_decode($input['content']);

        $page = Page::create($input);

        return $page;
    }

    /**
     * Update the specified page in database.
     *
     * @param $input
     */
    public function update($input)
    {
        $input['url'] = env('APP_URL') . '/' . $input['permalink'];
        $input['is_published'] = $input['is_published'] ?? 0;
        $input['content'] = html_entity_decode($input['content']);

        $page = Page::findOrFail($input['id']);

        $page->fill($input)->save();

        return $page;
    }
}
