## Repository Installtion Guide

1. Access and clone the repository in Bitbucket: https://bitbucket.org/commude_git/2.0_promon/src/development/
2. create new file as **[.env]** and copy all the contents of **[.env.example]** file to it.
3. run `composer install`
4. In your **[.env]** file set the `APP_URL` and `UPLOAD_URL` in your local domain.
-  `APP_URL` - ex. http://127.0.0.1:8000
-  `UPLOAD_URL` - ex. http://127.0.0.1:8000/storage

5. run to generate app key: `php artisan key:generate`
6. run to create a link for storage: `php artisan storage:link`
7. run to clear configs and caches: `php artisan config:cache`
8. run migration: `php artisan migrate:fresh --seed`
9. run local server: `php artisan serve`

## Post Development Scripts
- Run Lint - `php artisan lint:fix`

## Test Servers
- **[CMS Page](https://ae109dnoao.smartrelease.jp/honda_promon_front/public/login)**

## System Information
1. LARAVEL **[5.8](https://laravel.com/docs/5.8/installation)**  
2. designed and developed for **PHP 7.2** Environment
3. MariaDB MySQL
