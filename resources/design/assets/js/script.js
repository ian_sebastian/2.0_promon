
/***************************
	NICOLE 171208
	Sidebar Sub Menu Toggle
***************************/

function sidebar_submenu() {
	$('.dropdown').on('click', function(){
		$(this).find('.submenu').fadeToggle();
		if($(this).hasClass('js-dropdown')){
			$(this).removeClass('js-dropdown');
		}
		else{
			$(this).addClass('js-dropdown');
		}
	});
}

/***************************
	NICOLE 171208
	Question Toggle
***************************/

function questionToggle() {
	$('.question .section__row .buttonContainer a .toggleArrow').on('click', function (){
		$(this).toggleClass('js-toggle--arrow');
		$(this).parents('.section__row').next('.section__content').toggleClass('js-section__content');
		$(this).parents('.section__row').next('.section__content').slideToggle('fast');
	});
	$('.padded .section__row .buttonContainer a .toggleArrow').on('click', function (){
		$(this).toggleClass('js-toggle--arrow');
		$(this).parents('.section__row').next('.section__content').toggleClass( 'js-section__content');
		$(this).parents('.section__row').next('.section__content').slideToggle('fast');
	});
}

function viewport(){
	var _ua = (function(u){
	  return {
	    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
	      || u.indexOf("ipad") != -1
	      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
	      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
	      || u.indexOf("kindle") != -1
	      || u.indexOf("silk") != -1
	      || u.indexOf("playbook") != -1,
	    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
	      || u.indexOf("iphone") != -1
	      || u.indexOf("ipod") != -1
	      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
	      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
	      || u.indexOf("blackberry") != -1
	  }
	})(window.navigator.userAgent.toLowerCase());
	if(_ua.Tablet){
		//for this code only apply tablet
		$("meta[name='viewport']").attr('content', 'width=1100');
	}
}

function clickableRow(){
    $(".clickableRow").click(function() {
        window.location = $(this).data("href");
    });
}

function numCounter(){
	$(".incremental-counter").incrementalCounter({
	  "digits": 5
	});
}

function chart(){
	if ($("#myChart").length) {
		var ctx = document.getElementById('myChart').getContext('2d');
		var chart = new Chart(ctx, {
		    // The type of chart we want to create
		    type: 'line',

		    // The data for our dataset
		    data: {
		        labels: ["4月8日", "4月12日", "4月15日", "4月17日", "4月19日",  "4月22日", "4月26日"],
		        datasets: [{
		            borderColor: 'rgba(39, 117, 246, 1)',
		            data: [100, 450, 300, 800, 1120, 1200, 780],
		        }]
		    },

		    // Configuration options go here
		    options: {}
		});
	}
}

function clone_elements() {
	if ($(".question").length) {
		var rowNum = $('.question').length;
		$(".addmore_questions").click(function () {
		      rowNum = rowNum + 1;
		      var question = $('.section.question').last();
		      var last = $('.question').last().attr('id');
		      var nextHtml = question.clone(true);
		      nextHtml.attr('id', 'question' + rowNum);
		      question.after(nextHtml);
		      console.log($('#'+ last +' select').val());
		      $('#question'+rowNum+' select').val($('#'+ last +' select').val());
		 });
	}
	if ($("#config-field").length) {
		var rowNum = $('#config-field .section__configRowContainer').length;
		$("#config-field .section__configRowContainer .section__configRow .txtadd a").click(function () {
		      rowNum = rowNum + 1;
		      var question = $('#config-field .section__configRowContainer').last();
		      $("#config-field .section__configRowContainer .section__configRow .txtadd").addClass('hide');
		      var nextHtml = question.clone(true);
		      nextHtml.attr('id', 'field01-' + rowNum);
		      question.after(nextHtml);
		      $('#field01-'+ rowNum +' .section__configRow .txtadd').removeClass('hide');
		 });
	}
	if ($("#config-userlink").length) {
		var rowNum = $('#config-userlink .section__configRowContainer').length;
		$("#config-userlink .section__configRowContainer .section__configRow .txtadd a").click(function () {
		      rowNum = rowNum + 1;
		      var question = $('#config-userlink .section__configRowContainer').last();
		      $("#config-userlink .section__configRowContainer .section__configRow .txtadd").addClass('hide');
		      var nextHtml = question.clone(true);
		      nextHtml.attr('id', 'field02-' + rowNum);
		      question.after(nextHtml);
		      $('#field02-'+ rowNum +' .section__configRow .txtadd').removeClass('hide');
		 });
	}
	if ($("#config-security").length) {
		var rowNum = $('#config-security .section__configRowContainer').length;
		$("#config-security .section__configRowContainer .section__configRow .txtadd a").click(function () {
		      rowNum = rowNum + 1;
		      var question = $('#config-security .section__configRowContainer').last();
		      $("#config-security .section__configRowContainer .section__configRow .txtadd").addClass('hide');
		      var nextHtml = question.clone(true);
		      nextHtml.attr('id', 'field03-' + rowNum);
		      question.after(nextHtml);
		      $('#field03-'+ rowNum +' .section__configRow .txtadd').removeClass('hide');
		 });
	}
}

function delete_elements() {
	if ($(".section__configRowContainer").length) {
		$(".section__configRowContainer .section__configRow .txtdel a").click(function (e) {
			var rowNum = $(this).parents('.txtdel').parents('.section__configRow').parents('.section__configRowContainer').parents('form').children('.section__configRowContainer').length;
			if (rowNum != 1) {
				configCat = $(this).closest('.section.form.section__config.active.visible').attr('id');
				$(this).closest('.section__configRowContainer').remove();
		      	var configId = $("#" + configCat).children('.section__configInner').children('form').children('.section__configRowContainer').last().attr('id');
		      	$("#" + configId).children('.section__configRow').children('.txtadd').removeClass('hide');
			}
		});
	}
	if ($(".buttonContainer").length) {
		$(".buttonContainer a .closeBtn").click(function (e) {
			var rowNum = $(this).parents('a').parents('.buttonContainer').parents('.section__row').parents('.section.question').parents('form').children('.section.question').length;
			if (rowNum != 1) {
				$(this).closest('.section.question').remove();
			}
		});
	}
}

function form_type() {
	if($('.question__type').length) {
		//array construction and appending of custom types
		var customType = [];
		if ($(".custom__typeList .custom__type").length) {
			z = 0;
			$('.custom__type').each(function () {
				customTypeMasterName = $(this).data('customtype_name');
				customType.push({
					mastername: customTypeMasterName,
					fields : []
				});
				$(".question__type").append('<option data-fieldtype="'+customTypeMasterName+'" value="'+customTypeMasterName+'">'+customTypeMasterName+'</option>').val('');
				$(this).children('.custom__typeField').each(function () {
					customTypeFieldName = $(this).children('.custom__typeFieldName').val();
					customTypeFieldType = $(this).children('.custom__typeFieldType').val();
					customType[z].fields.push({'fieldname': customTypeFieldName, 'fieldtype': customTypeFieldType});
				});
				z++;
			});
			$(".question__type ").val($(".question__type option:first").val());
		}
		// console.log(customType);
		// console.log(customType.length);
		$('.question__type').change(function(e) {
			var clicked = $(e.target)
			$(this).parents('.td').parents('.questiontypeField').siblings('.fieldtypelayout').remove();
			var rowNum = $(this).parents('.td').parents('.questiontypeField').parents('.section__content--table').parents('.section__content').parents('.section').attr('id');
			rowNum = rowNum.replace(/\D/g,'');
			var selected = $(this).find('option:selected');
			var extra = selected.data('fieldtype');
			if(extra == 'text' || extra == 'textarea' || extra == 'checkbox' || extra == 'radiobutton' || extra == 'select') {
				var radiocheckbox = ''+
				'<div class="tr fieldlabel fieldtypelayout">'+
				'<div class="th">'+extra+'</div>'+
				'</div>'+
				'<div class="tr checkboxlayout fieldtypelayout">'+
				'<div class="th">レイアウト</div>'+
				'<div class="td">'+
				'<div class="row">'+
				'<div class="custom__radioBtnbox">'+
				'<label>'+
				'<input type="radio" name="choice_arrangement_'+ rowNum +'_0" value="0">'+
				'<span class="btn"></span>'+
				'<p>横に並べる</p>'+
				'</label>'+
				'</div>'+
				'<div class="custom__radioBtnbox">'+
				'<label>'+
				'<input type="radio" name="choice_arrangement_'+ rowNum +'_0" value="1">'+
				'<span class="btn"></span>'+
				'<p>縦に並べる</p>'+
				'</label>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="tr choicesFields fieldtypelayout">'+
				'<div class="th">選択肢</div>'+
				'<div class="td">'+
				'<div>'+
				'<textarea name="choices_'+ rowNum +'"></textarea>'+
				'</div>'+
				'<div class="custom__checkbox2">'+
				'<label>'+
				'<input type="checkbox" name="checkbox02_'+ rowNum +'_0">'+
				'<span class="btn"></span>'+
				'<p>　選択肢に「その他」を追加する</p>'+
				'</label>'+
				'</div>'+
				'<div class="custom__checkbox2">'+
				'<label>'+
				'<input type="checkbox" name="checkbox03_'+ rowNum +'_0">'+
				'<span class="btn"></span>'+
				'<p>　「その他」の内容をひ入力必須にする</p>'+
				'</label>'+
				'</div>'+
				'<div class="custom__textfield">'+
				'<p>テキストフィールドのサイズ</p>'+
				'<input type="text" name="textfieldsize_'+ rowNum +'_0">'+
				'<p>px</p>'+
				'</div>'+
				'</div>'+
				'</div>';
				var selection = ''+
				'<div class="tr fieldlabel fieldtypelayout">'+
				'<div class="th">'+extra+'</div>'+
				'</div>'+
				'<div class="tr choicesFields fieldtypelayout">'+
				'<div class="th">選択肢</div>'+
				'<div class="td">'+
				'<div>'+
				'<textarea name="choices_'+ rowNum +'_0"></textarea>'+
				'</div>'+
				'</div>'+
				'</div>';
				var textareatext = ''+
				'<div class="tr fieldlabel fieldtypelayout">'+
				'<div class="th">'+extra+'</div>'+
				'<div class="td">'+
				'<p>'+extra+'</p>'+
				'</div>'+
				'</div>';
				if(extra == 'text') {
					$(this).parents('.td').parents('.questiontypeField').after(textareatext);
				}
				if(extra == 'textarea') {
					$(this).parents('.td').parents('.questiontypeField').after(textareatext);
				}
				if(extra == 'checkbox') {
					$(this).parents('.td').parents('.questiontypeField').after(radiocheckbox);
				}
				if(extra == 'radiobutton') {
					$(this).parents('.td').parents('.questiontypeField').after(radiocheckbox);
				}
				if(extra == 'select') {
					$(this).parents('.td').parents('.questiontypeField').after(selection);
				}
			}
			//custom field type construction
			var customtypetemplate = [];
			var customtypehtml = '';
			var k,j,arrayItem;
			for (k = 0; k < customType.length; ++k) {
				customtypehtml += ''+
				'<div class="tr fieldlabel fieldtypelayout">'+
				'<div class="th">'+customType[k].mastername+'</div>'+
				'</div>';
				for (j = 0; j < customType[k].fields.length; ++j) {
					if(customType[k].fields[j].fieldtype == 'text' || customType[k].fields[j].fieldtype == 'textarea' ){
						customtypehtml += ''+
						'<div class="tr fieldtypelayout">'+
						'<div class="th"><hr></div>'+
						'<div class="td"><hr></div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Name</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldname+'</p>'+
						'</div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Type</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldtype+'</p>'+
						'</div>'+
						'</div>';
					}
					if(customType[k].fields[j].fieldtype == 'radiobutton' || customType[k].fields[j].fieldtype == 'checkbox' ){
						customtypehtml += ''+
						'<div class="tr fieldtypelayout">'+
						'<div class="th"><hr></div>'+
						'<div class="td"><hr></div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Name</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldname+'</p>'+
						'</div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Type</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldtype+'</p>'+
						'</div>'+
						'</div>'+
						'<div class="tr checkboxlayout fieldtypelayout">'+
						'<div class="th">レイアウト</div>'+
						'<div class="td">'+
						'<div class="row">'+
						'<div class="custom__radioBtnbox">'+
						'<label>'+
						'<input type="radio" name="choice_arrangement_'+ rowNum +'_'+ j +'" value="0">'+
						'<span class="btn"></span>'+
						'<p>横に並べる</p>'+
						'</label>'+
						'</div>'+
						'<div class="custom__radioBtnbox">'+
						'<label>'+
						'<input type="radio" name="choice_arrangement_'+ rowNum +'_'+ j +'" value="1">'+
						'<span class="btn"></span>'+
						'<p>縦に並べる</p>'+
						'</label>'+
						'</div>'+
						'</div>'+
						'</div>'+
						'</div>'+
						'<div class="tr choicesFields fieldtypelayout">'+
						'<div class="th">選択肢</div>'+
						'<div class="td">'+
						'<div>'+
						'<textarea name="choices_'+ rowNum +'"></textarea>'+
						'</div>'+
						'<div class="custom__checkbox2">'+
						'<label>'+
						'<input type="checkbox" name="checkbox02_'+ rowNum +'_'+ j +'">'+
						'<span class="btn"></span>'+
						'<p>　選択肢に「その他」を追加する</p>'+
						'</label>'+
						'</div>'+
						'<div class="custom__checkbox2">'+
						'<label>'+
						'<input type="checkbox" name="checkbox03_'+ rowNum +'_'+ j +'">'+
						'<span class="btn"></span>'+
						'<p>　「その他」の内容をひ入力必須にする</p>'+
						'</label>'+
						'</div>'+
						'<div class="custom__textfield">'+
						'<p>テキストフィールドのサイズ</p>'+
						'<input type="text" name="textfieldsize_'+ rowNum +'_'+ j +'">'+
						'<p>px</p>'+
						'</div>'+
						'</div>'+
						'</div>';
					}
					if(customType[k].fields[j].fieldtype == 'select'){
						customtypehtml += ''+
						'<div class="tr fieldtypelayout">'+
						'<div class="th"><hr></div>'+
						'<div class="td"><hr></div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Name</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldname+'</p>'+
						'</div>'+
						'</div>'+
						'<div class="tr fieldlabel fieldtypelayout">'+
						'<div class="th">Field Type</div>'+
						'<div class="td">'+
						'<p>'+customType[k].fields[j].fieldtype+'</p>'+
						'</div>'+
						'</div>'+
						'<div class="tr choicesFields fieldtypelayout">'+
						'<div class="th">選択肢</div>'+
						'<div class="td">'+
						'<div>'+
						'<textarea name="choices_'+ rowNum +'_'+ j +'"></textarea>'+
						'</div>'+
						'</div>'+
						'</div>';
					}
				}
				customtypetemplate.push({
					templatehtml: customtypehtml
				});
				customtypehtml = '';
			}

			for (k = 0; k < customType.length; ++k) {
				// console.log(extra);
				// console.log(customType[k].mastername);
				// console.log(customtypetemplate[k].templatehtml);
				if(extra == customType[k].mastername) {
					$(clicked).parents('.td').parents('.questiontypeField').after(customtypetemplate[k].templatehtml);
				}
			}
		});
	}
}

function configCategory() {
	if ($(".js-configNav").length) {
		$('.configNav__item').click(function () {
			$('.configNav .td .configNav__item').removeClass('active');
			$(this).addClass('active');
			$('.section__config').removeClass('active');
			$('.section__config').removeClass('visible');
		    var configcat = $(this).attr("id");
	    	if(configcat=="configNav-details") {
				$('#config-details').addClass('active');
				setTimeout(function(){
					$('#config-details').addClass('visible').delay(1500);
				}, 1500);
			}
	    	if(configcat=="configNav-field") {
				$('#config-field').addClass('active');
				setTimeout(function(){
					$('#config-field').addClass('visible').delay(1500);
				}, 1500);
			}
	    	if(configcat=="configNav-userlink") {
				$('#config-userlink').addClass('active');
				setTimeout(function(){
					$('#config-userlink').addClass('visible').delay(1500);
				}, 1500);
			}
	    	if(configcat=="configNav-security") {
				$('#config-security').addClass('active');
				setTimeout(function(){
					$('#config-security').addClass('visible').delay(1500);
				}, 1500);
			}
		});
	}
}

function radioBtnClick(){
	$('.custom__radioBtnbox').on('click', function(){
		$(this).parent('.row').children('.custom__radioBtnbox').children('p').removeClass('checked');
		$(this).children('p').addClass('checked');
	});
}


$(function(){
	sidebar_submenu();
	questionToggle();
	viewport();
	clickableRow();
	numCounter();
	chart();
	clone_elements();
	delete_elements();
	configCategory();
	radioBtnClick();
	form_type();
}); // ready





$(window).on('load', function(){
}); // load





$(window).on('resize', function(){
}); // resize





$(window).scroll(function(){
}); // scroll





$(window).on('load resize', function(){
}); // load resize
