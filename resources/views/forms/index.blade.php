@extends('layouts.admin', [
    'title' => 'Promon | フォーム 一覧',
    'page_name' => 'form_index',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">フォーム</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="{{ route('forms.delete') }}">
@csrf
    <div class="section nosidepad">
        <div class="section__row">
            <div class="pages">
                <div class="thead all-pages">
                    <div class="tr">
                        <div class="td">
                            <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="page_all" id="selectAllBtn" value="1">
                                    <span class="btn"></span>
                                </label>
                            </div>
                            <div class="pages__Btn small"><button type="submit" id="btnSubmit" disabled><p>一括削除</p></button></div>
                        </div>
                        <div class="td"><p>&nbsp;</p></div>
                        <div class="td"><p>&nbsp;</p></div>
                        <div class="td"><div class="pages__Btn black"><a href="{{ route('forms.create') }}"><p>新規作成</p></a></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section form addPad2">
        <div class="pages">
            <div class="section__row">
                <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <td></td>
                            <td>フォームタイトル</td>
                            <td>フォームID</td>
                            <td>作成日</td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($forms as $form)
                    <tr>
                        <td>
                            <div class="custom__checkbox">
                                <label>
                                    <input type="checkbox" name="id[]" class="chkBoxId" value="{{ $form->id }}">
                                    <span class="btn"></span>
                                </label>
                            </div>
                        </td>
                        <td><p class="data__label">{{ $form->title }}</p></td>
                        <td><p class="data__label">{{ $form->code }}</p></td>
                        <td><p class="data__label">{{ $form->updated_at->format('Y年m月d日') }}</p></td>
                        <td><div class="pages__Btn"><a href="{{ route('forms.edit', $form->id) }}"><p>編集</p></a></div></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
@endsection
@push('scripts')
<script>
    $('#selectAllBtn').click(function(event) {
        $(".chkBoxId").prop('checked', $(this).prop('checked'));
        chkboxCheck();
    });

    $('.chkBoxId').change(function () {
        chkboxCheck();
    });

    function chkboxCheck()
    {
        var chkBox = $('input.chkBoxId:checkbox:checked').length > 0;
        if(chkBox){
            $('#btnSubmit').attr('disabled', false);
        } else {
            $('#btnSubmit').attr('disabled', true);
        }
    }
</script>
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
@endpush
