<!DOCTYPE html>
<html lang="ja">
@include('components.head')
<body>
    <div class="wrapper">
        @include('components.nav')
        <div id="main">
            @include('components.header')
            @yield('content')
            @include('components.footer')
        </div>
    </div> <!-- .wrapper -->
    <!-- scripts -->
    @include('partials.script')
</body>

</html>
