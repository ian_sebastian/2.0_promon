@extends('layouts.app', [
    'title' => 'Promon | 管理画面',
    'static_class' => 'login',
    'footer_class' => 'footer--login',])

@section('content')
<div class="login">
    <div class="login">
        <div class="logo">
            <figure>
                <img src="{{ asset('images/logo.svg') }}" alt="">
            </figure>
        </div>

        <form method="POST" action="{{ route('login.post') }}">
            @csrf
            <div class="form__wrapper">
                <div class="input user clearfix">
                    <figure>
                        <img src="{{ asset('images/icons/icon_login_user.png') }}" alt="">
                    </figure>
                    <input type="text" name="username" value="{{ old('username') }}">
                </div>
                <div class="input password clearfix">
                    <figure>
                        <img src="{{ asset('images/icons/icon_login_password.png') }}" alt="">
                    </figure>
                    <input type="password" name="password">
                </div>
                <div class="btn--wrapper">
                    <input type="submit" class="btn--login" value="ログイン">
                </div>

                <div class="loggedinbox">
                    <div class="custom__checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="btn"></span>
                        </label>
                    </div>
                    <p class="checkbox__label">ログイン情報を記憶する</p>
                </div>
            </div><!-- .form__wrapper -->
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
