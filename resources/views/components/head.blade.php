<head>
    <!-- <meta name="robots" content="noindex,nofollow"> -->
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $title ?? '' }}</title>
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/codemirror/codemirror.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdbootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdbootstrap/mdb.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pc.css') }}" rel="stylesheet" media="print,screen and (min-width: 768px)">
    <link href="{{ asset('css/sp.css') }}" rel="stylesheet" media="screen and (max-width: 767px)">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="shortcut icon" href="favicon.ico">
</head>
