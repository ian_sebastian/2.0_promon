<footer class="footer {{ $footer_class ?? ''  }}">
    <div class="footer__inner">
        @if (in_array(basename(request()->path()), ['login', '']))
        <p class="forgotpw">※パスワードを忘れた方は<a href="{{ route('password.request') }}">こちら</a href="#">から再発行してください。</p>
        @endif
        <p class="copyright">&copy; 2017 All Rights Reserved Commude Inc. is registered in Japan.</p>
    </div>
</footer>
