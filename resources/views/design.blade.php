@extends('layouts.admin', [
    'title' => 'Promon | デサイン',
    'page_name' => 'design',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">デサイン</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="{{ route('design.save') }}">
@csrf
    <div class="section form addPad">
        <div class="section__row">
            <textarea class="codemirror-textarea" name="content">{{ $masterCss->content }}</textarea>
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="btn__container">
        @if(Session::has('success_design'))
        <span class="success">{{ Session::get('success_design') }}</span>
        @endif
        <button type="submit" class="btn__container">更新</button>
    </div> <!-- .btn__container -->
</form>
@endsection
@push('scripts')
<script src="{{ asset('js/codemirror/codemirror.js') }}"></script>
<script src="{{ asset('js/codemirror/css.js') }}"></script>
<script src="{{ asset('js/codemirror.js') }}"></script>
<script>
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
