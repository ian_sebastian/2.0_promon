@extends('layouts.admin', [
    'title' => 'Promon | アナリティクス',
    'page_name' => 'analytics',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li class="active"><a href="#">アナリティクス</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="post" action="#">
@csrf
    <div class="section">
        <div class="analyticsSelect">
            <select class="analyticsSelect" id="select_title" name="id" onChange="onChange_Form_TitleSelection(this)">
                <option value="" selected disabled>選択してください</option>
                @foreach($forms as $form)
                <option value="{{ $form->id }}" {{ Request::route('id') == $form->id ? 'selected' : '' }}>{{ $form->title }}</option>
                @endforeach
            </select>
        <div>
        <div class="section__row">
            <div class="chart">
                <canvas id="formChart"></canvas>
            </div>
        </div>
    </div>
    <div class="section form">
        <div class="section__row">
            <p class="section__label title">合計応募件数</p>
        </div>
        <div class="section__row">
            <div class="label">
               <h3><span>合計</span>{{ $analytics['allSubmittedForms'] }}<span>件</span></h3>
            </div>
            <div class="label label2" style="float: right">
                <h3>{{ $analytics['submittedForms'] }}<span>件</span></h3>
            </div>
        </div>
    </div><!-- .section -->
</form>
@endsection
@push('scripts')

<script>
    let chart_labels = {!! $analytics['labels'] !!};
    let chart_datasets = {!! json_encode($datasets) !!};

    function onChange_Form_TitleSelection(e){
        let form = $(e).closest('form');
        var url = "{{ route('analytics.post', ['id' => '_id' ]) }}".replace('_id', e.value);
        form.attr('action', url);
        form.submit();
    };

    $(function(){
	    chart();
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{ asset('js/chart.js') }}"></script>
@endpush
