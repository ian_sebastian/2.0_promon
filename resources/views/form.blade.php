<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="description" content="{{ $page->meta_description }}">
    <meta name="keywords" content="{{ $page->meta_keyword }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $page->meta_title ?? $page->title }}</title>

    <!-- OG Share Website -->
    <meta property="og:url" content="{{ $page->ogp_url }}" />
    <meta property="og:type" content="{{ $page->ogp_type ?? 'website' }}" />
    <meta property="og:title" content="{{ $page->ogp_title }}" />
    <meta property="og:description" content="{{ $page->ogp_description }}" />
    <meta property="og:image" content="{{ $page->ogp_image }}" />
    <meta property="og:image:secure_url" content="{{ $page->ogp_image }}" />

    <!-- Twitter Share -->
    <meta name="twitter:title" content="{{ $page->social_twitter_title }}">
    <meta name="twitter:image" content="{{ $page->social_twitter_image }}">
    <meta name="twitter:card" content="{{ $page->social_twitter_card ?? 'summary' }}">

    <link rel="stylesheet" media="print,screen" href="{{ route('dynamicForm.showCSS') }}">
</head>
<body>
    <!-- Use Facebook SDK -->
    <script>
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=2455177381176867&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    {!! $page->content !!}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $('form').on('submit', function(e){
            $(this).attr('action', '{{ $page->url }}');
        });
        $('form').prepend('<input type="hidden" name="_token" value="{{ csrf_token() }}">')
        @if ($errors->any())
            alert('{{ $errors->first('*') }}');
        @endif
    </script>
</body>
</html>
