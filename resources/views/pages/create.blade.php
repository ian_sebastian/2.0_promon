@extends('layouts.admin', [
    'title' => 'Promon | ページ作成',
    'page_name' => 'page_create',
])

@section('content')
<div class="breadcrumbs__container clearfix">
    <div class="breadcrumbs">
        <ul>
            <li>ページ</li>
            <li class="active"><a href="#">新規作成</a></li>
        </ul>
    </div>
</div> <!-- .breadcrumbs__container -->
<form method="POST" action="{{ route('pages.save', $page['id'] ?? null ) }}" id="pageForm">
    @csrf
    <div class="section">
        <div class="section__row">
            <p class="section__label">タイトル<i class="required">*</i></p>
            <div class="section__subContent">
                <input type="text" name="title" placeholder="PROMON" value="{{ $page['title'] ?? '' }}">
            </div>
            <div class="section__subContent">
                <div class="custom__checkbox">
                    <label>
                        <input type="checkbox" name="is_published" value="1" {{ isset($page['is_published']) && $page['is_published'] == '1' ? 'checked' : '' }}>
                        <span class="btn"></span>
                    </label>
                </div>
                <p class="section__checkbox--label">活性化した</p>
            </div>
        </div>
    </div>
    <div class="section nobg">
        <p class="section__label permalink">パーマリンク:<i class="required">*</i> {{ env('APP_URL') }}/ </p> <input type="text" name="permalink" value="{{ $page['permalink'] ?? '' }}">
    </div>
    <div class="section nobg">
        <textarea id="content-area" class="section__row site__preview" name="content">{{ html_entity_decode($page['content'] ?? '') }}</textarea>
    </div>
    <div class="section form">
        <div class="section__row">
            <p class="section__label title">OGP設定</p>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr">
                    <div class="th">Title</div>
                    <div class="td">
                        <input type="text" name="ogp_title" value="{{ $page['ogp_title'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Type</div>
                    <div class="td">
                        <input type="text" name="ogp_type" value="{{ $page['ogp_type'] ?? '' }}" placeholder="website">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">URL</div>
                    <div class="td">
                        <input type="text" name="ogp_url" value="{{ $page['ogp_url'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Img ull</div>
                    <div class="td">
                        <input type="text" name="ogp_image" value="{{ $page['ogp_image'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Site name</div>
                    <div class="td">
                        <input type="text" name="ogp_site_name" value="{{ $page['ogp_site_name'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Description</div>
                    <div class="td">
                        <input type="text" name="ogp_description" value="{{ $page['ogp_description'] ?? '' }}">
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="section form">
        <div class="section__row">
            <p class="section__label title">Facebook</p>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr">
                    <div class="th">App ID</div>
                    <div class="td">
                        <input type="text" name="social_fb_app_id" value="{{ $page['social_fb_app_id'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Admin ID</div>
                    <div class="td">
                        <input type="text" name="social_fb_admin_id" value="{{ $page['social_fb_admin_id'] ?? '' }}">
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="section form">
        <div class="section__row">
            <p class="section__label title">Twitter</p>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr">
                    <div class="th">Title</div>
                    <div class="td">
                        <input type="text" name="social_twitter_title" value="{{ $page['social_twitter_title'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Image</div>
                    <div class="td">
                        <input type="text" name="social_twitter_image" value="{{ $page['social_twitter_image'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Card</div>
                    <div class="td">
                        <input type="text" name="social_twitter_card" value="{{ $page['social_twitter_card'] ?? '' }}" placeholder="summary">
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="section form">
        <div class="section__row">
            <p class="section__label title">Meta Tag</p>
        </div>
        <div class="section__content">
            <div class="section__content--table">
                <div class="tr">
                    <div class="th">Title</div>
                    <div class="td">
                        <input type="text" name="meta_title" value="{{ $page['meta_title'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Description</div>
                    <div class="td">
                        <input type="text" name="meta_description" value="{{ $page['meta_description'] ?? '' }}">
                    </div>
                </div>
                <div class="tr">
                    <div class="th">Keyword</div>
                    <div class="td">
                        <input type="text" name="meta_keyword" value="{{ $page['meta_keyword'] ?? '' }}">
                    </div>
                </div>
            </div> <!-- .section__content--table -->
        </div> <!-- .section__content -->
    </div><!-- .section -->
    <div class="btn__container">
        <button type="submit" class="btn__container">更新</button>
    </div> <!-- .btn__container -->
</form>

@endsection
@push('scripts')
<script src="{{ asset('js/tinymce-plugins/tinymce.js') }}"></script>
<script src="{{ asset('js/tinymce.js') }}"></script>
<script>
@if ($errors->any())
  alert('{{ $errors->first('*') }}');
@endif
</script>
@endpush
