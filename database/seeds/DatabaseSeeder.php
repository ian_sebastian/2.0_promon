<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $this->call(CampaignTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MasterCssTableSeeder::class);
    }
}
