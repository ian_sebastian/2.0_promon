<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::insert([
            'username' => 'root',
            'email' => 'admin@promon.com',
            'password' => Hash::make('password'),
        ]);
    }
}
