<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionFormsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('question_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('form_id');
            $table->unsignedBigInteger('master_input_id');
            $table->string('type');
            $table->string('question');
            $table->string('before_text')->nullable();
            $table->string('after_text')->nullable();
            $table->boolean('is_required')->default(0);
            $table->boolean('has_others')->default(0);
            $table->timestamps();

            //foreign keys
            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
            $table->foreign('master_input_id')->references('id')->on('master_inputs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('question_forms');
    }
}
